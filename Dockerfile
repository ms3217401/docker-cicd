FROM python:3-alpine
ENV PYTHONUNBUFFERED 1
WORKDIR /code
COPY . /code
RUN pip install -r requirements.txt
RUN mkdir db

EXPOSE 3030
CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000
